<?php

namespace Database\Seeders;

use App\Models\TiposDepartamento;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TiposDepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        TiposDepartamento::create(['descricao' => 'Diretoria']);
        TiposDepartamento::create(['descricao' => 'Setor']);
    }
}
