<?php

namespace Database\Seeders;

use App\Models\StatusProtocolo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StatusProtocoloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        StatusProtocolo::create(['descricao' => 'ARQUIVADO']);
        StatusProtocolo::create(['descricao' => 'AGUARDANDO ANÁLISE']);
        StatusProtocolo::create(['descricao' => 'FINALIZADO']);
    }
}
