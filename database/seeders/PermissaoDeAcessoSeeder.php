<?php

namespace Database\Seeders;

use App\Models\PermissaoDeAcesso;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PermissaoDeAcessoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        PermissaoDeAcesso::create(['nivel' => 'admin']);
        PermissaoDeAcesso::create(['nivel' => 'protocolo']);
        PermissaoDeAcesso::create(['nivel' => 'usuario']);
    }
}
