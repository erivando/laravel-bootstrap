<?php

namespace Database\Seeders;

use App\Models\Origens;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OrigensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Origens::create(['origem' => 'OUTRO']);
        Origens::create(['origem' => 'TANCREDO NEVES PRESIDENTE EMEF']);
        Origens::create(['origem' => 'FRANCISCA FLORENCIA DA SILVA PROF° EMEIEF']);
        Origens::create(['origem' => 'ALMIR FREITAS DUTRA EMEF']);
        Origens::create(['origem' => 'POVO PITAGUARI EMIEB']);
        Origens::create(['origem' => 'ANTONIO GONDIM DE LIMA EMEIEF']);
        Origens::create(['origem' => 'BRAZ RIBEIRO DA SILVA EMEF']);
        Origens::create(['origem' => 'CARLOS JEREISSATI SEN EMEIEF']);
        Origens::create(['origem' => 'CESAR CALS DE O FILHO EMEIEF GOV']);
        Origens::create(['origem' => 'CESAR CALS NETO EMEIEF']);
        Origens::create(['origem' => 'DURVAL AIRES JORNALISTA EMEF']);
        Origens::create(['origem' => 'EDWIRGES SANTA EMEIEF']);
        Origens::create(['origem' => 'EVANDRO AYRES DE MOURA EMEF']);
        Origens::create(['origem' => 'FRANCISCO ANTONIO FONTENELE EMEIEF ']);
        Origens::create(['origem' => 'FCO BARBOSA COMISSARIO EMEF']);
        Origens::create(['origem' => 'GENCIANO GUERREIRO DE BRITO EMEF']);
        Origens::create(['origem' => 'JARI EMEIEF']);
        Origens::create(['origem' => 'JATOBA EMEF']);
        Origens::create(['origem' => 'JOAQUIM AGUIAR EMEF']);
        Origens::create(['origem' => 'JOSE BELISARIO DE SOUSA EMEF']);
        Origens::create(['origem' => 'JOSE DE BORBA VASCONCELOS DR EMEF']);
        Origens::create(['origem' => 'JOSE EMEF INSTITUTO SAO']);
        Origens::create(['origem' => 'JOSE MARIO BARBOSA EMEIEF']);
        Origens::create(['origem' => 'JOSE NOGUEIRA MOTA EMEIFE']);
        Origens::create(['origem' => 'LICEU DE MARACANAU EMEF']);
        Origens::create(['origem' => 'MARIA PEREIRA DA SILVA EMEF']);
        Origens::create(['origem' => 'MANOEL GOMES DE MORAIS EMEIEF']);
        Origens::create(['origem' => 'MANOEL MOREIRA LIMA EMEIEF']);
        Origens::create(['origem' => 'MANOEL ROSEO LANDIM EMEIEF']);
        Origens::create(['origem' => 'MIRIAN PORTO MOTA CRECHE FEBENCE']);
        Origens::create(['origem' => 'ADAUTO FERREIRA LIMA EMEIEF']);
        Origens::create(['origem' => 'NAPOLEAO BONAPARTE VIANA EMEIEF']);
        Origens::create(['origem' => 'NORBERTO ALVES BATALHA EMEIEF']);
        Origens::create(['origem' => 'PARQUE PIRATININGA EMEIEF']);
        Origens::create(['origem' => 'RODOLFO TEOFILO EMEF']);
        Origens::create(['origem' => 'RUI BARBOSA EMEF']);
        Origens::create(['origem' => 'ULYSSES GUIMARAES DEPUTADO EMEF']);
        Origens::create(['origem' => 'VALDENIA ACELINO DA SILVA EMEF']);
        Origens::create(['origem' => 'ALMIR FREITAS DUTRA PREF EMEIEF']);
        Origens::create(['origem' => 'ANTONIO DE ALBUQUERQUE SOUSA FILHO EMEIEF']);
        Origens::create(['origem' => 'LUIZ GONZAGA DOS SANTOS EMEF']);
        Origens::create(['origem' => 'CENTRO ED JOV E ADUL MARACANAU-CEJA']);
        Origens::create(['origem' => 'CENTRO DE EDUCACAO DE JOVENS E ADULTOS DE PAJUCARA']);
        Origens::create(['origem' => 'MARIA DE LOURDES SILVA PROFESSORA EMEF']);
        Origens::create(['origem' => 'CENTRO DE APOIO E DESENV DE EDUC ESPECIAL - CADEE']);
        Origens::create(['origem' => 'SITIO DO PICA-PAU AMARELO CENTRO DE EDUCACAO INFANTIL']);
        Origens::create(['origem' => 'APRENDER PENSANDO EMEF']);
        Origens::create(['origem' => 'SINFRONIO PEIXOTO DE MORAIS EMEIEF']);
        Origens::create(['origem' => 'JOSE MARTINS RODRIGUES DEPUTADO EMEIEF']);
        Origens::create(['origem' => 'RACHEL DE QUEIROZ EMEIEF']);
        Origens::create(['origem' => 'WALMIKI SAMPAIO DE ALBUQUERQUE EMEIEF']);
        Origens::create(['origem' => 'MANOEL RODRIGUES PINHEIRO DE MELO EMEIEF']);
        Origens::create(['origem' => 'ALEGRIA CULTURAL EMEIEF']);
        Origens::create(['origem' => 'PENSANDO E CONSTRUINDO EMEF']);
        Origens::create(['origem' => 'TERRA DO SABER EMEF']);
        Origens::create(['origem' => 'RAIZES E ASAS EMEF']);
        Origens::create(['origem' => 'INTEGRANDO O SABER EMEIEF']);
        Origens::create(['origem' => 'CONSTRUINDO O SABER EMEIEF']);
        Origens::create(['origem' => 'JULIO CESAR COSTA LIMA I EMEIEF']);
        Origens::create(['origem' => 'JOAO MAGALHAES DE OLIVIERA EMEIEF']);
        Origens::create(['origem' => 'ELIAS SILVA OLIVEIRA EMEIEF']);
        Origens::create(['origem' => 'MARIA ROCHELLE DA SILVA EMEF']);
        Origens::create(['origem' => 'OSMIRA EDUARDO DE CASTRO CRECHE']);
        Origens::create(['origem' => 'HEITOR VILLA LOBOS EMEIEF']);
        Origens::create(['origem' => 'CORA CORALINA - EMEIEF']);
        Origens::create(['origem' => 'VINICIUS DE MORAIS EMEF']);
        Origens::create(['origem' => 'HERBERT JOSE DE SOUZA BETINHO EMEF']);
        Origens::create(['origem' => 'DULCE IRMA EMEF']);
        Origens::create(['origem' => 'MARIA MARQUES DO NASCIMENTO EMEF']);
        Origens::create(['origem' => 'JOSE DANTAS SOBRINHO EMEF']);
        Origens::create(['origem' => 'TERESA DE CALCUTA EMEF MADRE']);
        Origens::create(['origem' => 'CARLOS DRUMMOND DE ANDRADE EMEIEF']);
        Origens::create(['origem' => 'HELDER PESSOA CAMARA DOM EMEIEF']);
        Origens::create(['origem' => 'ELEAZAR DE CARVALHO MAESTRO EMEF']);
        Origens::create(['origem' => 'MARIO COVAS GOVERNADOR EMEIEF']);
        Origens::create(['origem' => 'EDSON QUEIROZ EMEF']);
        Origens::create(['origem' => 'MARIA JOSE ISIDORO PROF° EMEIEF']);
        Origens::create(['origem' => 'LUIS CARLOS PRESTES EMEIEF']);
        Origens::create(['origem' => 'ADELIA SANTOS DE SOUSA PROF° EMEIEF']);
        Origens::create(['origem' => 'NARCISO PESSOA DE ARAUJO EMEIEF']);
        Origens::create(['origem' => 'FRANCISCO OSCAR RODRIGUES PROF° EMEIEF']);
        Origens::create(['origem' => 'MARIA DO SOCORRO VIANA FREITAS PROF° EMEIEF']);
        Origens::create(['origem' => 'NORMA CELIA PINHEIRO CRISPIM PROF° EMEIEF']);
        Origens::create(['origem' => 'CEZARINA DE OLIVEIRA GOMES PROF° EMEIEF']);
        Origens::create(['origem' => 'FRANCISCO ARAUJO DO NASCIMENTO PROF° EMEIEF']);
        Origens::create(['origem' => 'MARIA DE JESUS DE SOUSA MACAMBIRA PROF° EMEIEF']);
        Origens::create(['origem' => 'MARIA LIDUINA NUNES DE SENA PROF° EMEIEF']);
        Origens::create(['origem' => 'PAULO FREIRE PROF° EMEIEF']);
        Origens::create(['origem' => 'MARIA MARQUES CEDRO PROF° EMEIEF']);
        Origens::create(['origem' => 'MARIA GLAUCIA MENEZES TEIXEIRA ALBUQUERQUE PROF° EMEIEF']);
        Origens::create(['origem' => 'ANA BEATRIZ MACEDO TAVARES MARQUES ESTUDANTE EMEF']);
        Origens::create(['origem' => 'MARIA JOSÉ HOLANDA DO VALE PROF° EMEIEF']);
        Origens::create(['origem' => 'NOSSA SENHORA DE FÁTIMA CHECHE MUNICIPAL']);
        Origens::create(['origem' => 'PGM']);
        Origens::create(['origem' => 'SRHP']);
        Origens::create(['origem' => 'PREFEITURA DE MARACANAÚ - GAB']);
    }
}
