<?php

namespace Database\Seeders;

use App\Models\Departamentos;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Ensino e aprendizagem - DEA',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria Geral de Administração',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Avaliação',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Planejamento',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'GABS',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'DETRE',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'CME',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 1,
            'nome' => 'Outra',
        ]);
        Departamentos::create([
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Gestão Educacional',
        ]);
        Departamentos::create([
            'departamento_id' => 1,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Desenvolvimento Curricular',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Setor de Alimentação Escolar',
        ]);
        Departamentos::create([
            'departamento_id' => 1,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Mídias Educacionais',
        ]);
        Departamentos::create([
            'departamento_id' => 1,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Gestão Escolar',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Protocolo',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Gestão de Pessoas',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Manutenção e Segurança do Parque Escolar',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Reprografia',
        ]);
        Departamentos::create([
            'departamento_id' => 4,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Tecnologia da Informática',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Estatística Educacional',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Suporte Técnico às Unidades Executoras',
        ]);
        Departamentos::create([
            'departamento_id' => 1,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Ensino e Aprendizagem',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria Geral de Administração',
        ]);
        Departamentos::create([
            'departamento_id' => 3,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Avaliação',
        ]);
        Departamentos::create([
            'departamento_id' => 4,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Planejamento',
        ]);
        Departamentos::create([
            'departamento_id' => 5,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'GABS',
        ]);
        Departamentos::create([
            'departamento_id' => 6,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'DETRE',
        ]);
        Departamentos::create([
            'departamento_id' => 7,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'CME',
        ]);
        Departamentos::create([
            'departamento_id' => 8,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 1,
            'nome' => 'Outro',
        ]);
        Departamentos::create([
            'departamento_id' => 1,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Programas Sócioeducativos - Grupos de Trabalho',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Serviços Gerais',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Avaliação de Desempenho e Progresso',
        ]);
        Departamentos::create([
            'departamento_id' => 4,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Planejamento de Políticas Públicas em Educação',
        ]);
        Departamentos::create([
            'departamento_id' => 4,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Planejamento Financeiro, Orçamentário e Controle Interno',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Transporte',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Setor de Material e Patrimônio',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Administração Escolar',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Patrimônio',
        ]);
        Departamentos::create([
            'departamento_id' => 2,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Engenharia',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 1,
            'departamento_externo' => 0,
            'nome' => 'Diretoria de Gestão Educacional',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Setor de Apoio ao Educando',
        ]);
        Departamentos::create([
            'departamento_id' => 9,
            'tipos_departamento_id' => 2,
            'departamento_externo' => 0,
            'nome' => 'Creches Contratadas',
        ]);
    }
}
