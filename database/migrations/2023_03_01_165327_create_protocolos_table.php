<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('protocolos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('status_protocolo_id')->constrained('status_protocolos');
            $table->foreignId('origem_id')->constrained('origens');
            $table->string('codigo');
            $table->text('descricao');
            $table->text('resolucao')->nullable();
            $table->string('origem_outro', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('protocolos');
    }
};
