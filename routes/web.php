<?php

use App\Http\Controllers\DepartamentosController;
use App\Http\Controllers\OrigensController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('app');
});
Route::get('/origens', [OrigensController::class, 'index']);
Route::get('/departamentos', [DepartamentosController::class, 'index']);