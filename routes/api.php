<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DepartamentosController;
use App\Http\Controllers\OrigensController;
use App\Http\Controllers\ProtocoloController;
use App\Http\Controllers\RelatorioController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum'])->group(function() {
    Route::get('/origens', [OrigensController::class, 'index']);
    Route::get('/departamentos', [DepartamentosController::class, 'index']);

    Route::middleware('permissaoProtocolo')->group(function() {
        Route::get('/protocolos', [ProtocoloController::class, 'index']);
        Route::post('/protocolos', [ProtocoloController::class, 'store']);
        Route::put('/protocolo/editar/{id}', [ProtocoloController::class, 'updateProtocolo']);
    });
    
    Route::middleware('permissaoUsuario')->group(function() {
        Route::get('/protocolos-setor', [ProtocoloController::class, 'protocolosPorSetor']);
        Route::get('/protocolos-finalizados-setor', [ProtocoloController::class, 'protocolosFinalizadoPorSetor']);
        Route::put('/protocolo/finalizar/{id}', [ProtocoloController::class, 'finalizaProtocolo']);
        Route::post('/protocolo/encaminhar/{id}', [ProtocoloController::class, 'encaminhaProtocolo']);
    });
    
    Route::get('/protocolo/{id}', [ProtocoloController::class, 'show']);

    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/perfil', [AuthController::class, 'perfil']);
});

Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/relatorio', [RelatorioController::class, 'index']);
Route::get('/pesquisa', [ProtocoloController::class, 'pesquisaProtocolo']);