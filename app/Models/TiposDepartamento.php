<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiposDepartamento extends Model
{
    use HasFactory;
    protected $table = 'tipos_departamento';
    protected $fillable = ['descricao'];

    public function departamentos()
    {
        return $this->hasOne(Departamentos::class, 'tipos_departamento_id', 'id');
    }
}
