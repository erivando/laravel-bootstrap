<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
class Usuario extends Model
{
    use HasFactory, HasApiTokens, Notifiable;
    protected $table = 'usuarios';
    protected $fillable = [
        'departamento_id',
        'permissao_id',
        'usuario',
        'senha',
        'nome',
        'cpf',
        'ativo'
    ];

    public function movimentacao()
    {
        return $this->hasOne(Movimentacao::class);
    }

    public function departamento()
    {
        return $this->belongsTo(Departamentos::class);
    }
}
