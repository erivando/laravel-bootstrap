<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Origens extends Model
{
    use HasFactory;
    protected $fillable = ['origem', 'ativo'];

    public function protocolo()
    {
        return $this->hasOne(Protocolo::class);
    }
}
