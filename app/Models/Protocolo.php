<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Protocolo extends Model
{
    use HasFactory;
    protected $table = 'protocolos';
    protected $fillable = [
        'status_protocolo_id',
        'origem_id',
        'codigo',
        'descricao',
        'resolucao',
        'origem_outro'
    ];

    public function movimentacao()
    {
        return $this->hasMany(Movimentacao::class, 'protocolo_id', 'id');
    }

    public function statusProtocolo()
    {
        return $this->belongsTo(StatusProtocolo::class, 'status_protocolo_id', 'id');
    }

    public function origem()
    {
        return $this->belongsTo(Origens::class, 'origem_id', 'id');
    }

    public function primeiraMovimentacao()
    {
        return $this->hasOne(Movimentacao::class);
    }

    public function movimentacaoDepartamento()
    {
        return $this->hasOne(Movimentacao::class);
    }

    public function ultimaMovimentacao()
    {
        return $this->hasOne(Movimentacao::class)->latestOfMany();
    }

    static function protocolosPorIdSetor()
    {
        $departamentoDoUsuarioLogado = auth()->user()->departamento_id;

        $movimentacoes = DB::select(
            'SELECT protocolo.id AS protocolo_id, movimentacao.departamento_destino_id AS departamento_id FROM `protocolos` AS protocolo   
                JOIN movimentacoes AS movimentacao
                ON movimentacao.protocolo_id = protocolo.id
                WHERE protocolo.status_protocolo_id = 2
                    ORDER BY protocolo.id, movimentacao.created_at DESC;',
        );

        $protocolosId = collect([]);
        
        collect($movimentacoes)->groupBy('protocolo_id')->transform(function ($item) use($protocolosId, $departamentoDoUsuarioLogado ) {
            $movimentacao = $item->first();
            if($movimentacao->departamento_id == $departamentoDoUsuarioLogado) {
                $protocolosId->push($movimentacao->protocolo_id);
            }
        });
        return $protocolosId;
    }

    static function filtro($request)
    {
        $query = Protocolo::query();
        if($request->has('codigo')) {
            $query->where('codigo', $request['codigo']);
        }
        
        if($request->has('origem')) {
            // $query->with('origem')->where('origem', 'LIKE', '%'.$request['origem'].'%');
            $query->with('origem')
                ->whereHas('origem', fn($query) => $query->where('origem', 'LIKE', '%'.$request['origem'].'%'));
        }
        // dd($query);
        return $query;
    }
}
