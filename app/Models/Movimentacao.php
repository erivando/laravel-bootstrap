<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimentacao extends Model
{
    use HasFactory;
    protected $table = 'movimentacoes';
    protected $fillable = [
        'usuario_id',
        'protocolo_id',
        'departamento_origem_id',
        'departamento_destino_id',
        'descricao'
    ];

    public function protocolo()
    {
        return $this->belongsTo(Protocolo::class, 'protocolo_id', 'id');
    }

    public function departamentoOrigem()
    {
        return $this->belongsTo(Departamentos::class, 'departamento_origem_id', 'id');
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'usuario_id', 'id');
    }

    public function departamentoDestino()
    {
        return $this->belongsTo(Departamentos::class, 'departamento_destino_id', 'id');
    }

    public function movimentacaoDepartamento()
    {
        return $this->belongsTo(Departamentos::class, 'departamento_origem_id', 'id');
    }
}
