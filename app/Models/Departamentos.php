<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    use HasFactory;
    protected $table = 'departamentos';
    protected $filable = ['departamento_id', 'tipos_departamento_id', 'departamento_externo', 'nome'];

    public function departamentos()
    {
        return $this->hasOne(Departamentos::class, 'departamento_id', 'id');
    }

    public function departamento()
    {
        return $this->belongsTo(Departamentos::class, 'departamento_id', 'id');
    }

    public function tiposDepartamento()
    {
        return $this->belongsTo(TiposDepartamento::class);
    }
    
    public function movimentacao()
    {
        return $this->hasOne(Movimentacao::class);
    }

    public function departamentoOrigem()
    {
        return $this->hasOne(Movimentacao::class, 'departamento_origem_id', 'id');
    }

    public function departamentoDestino()
    {
        return $this->hasOne(Movimentacao::class, 'departamento_destino_id', 'id');
    }

    public function usuario()
    {
        return $this->hasOne(Usuario::class, 'departamento_id', 'id');
    }
}
