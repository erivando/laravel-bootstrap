<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\Usuario;
use Exception;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        try {
            $usuario = Usuario::where('usuario', $request->usuario)->where('ativo', true)->first();
            if (!$usuario || !Hash::check($request->senha, $usuario->senha)) {
                return throw new Exception('Usuario e/ou senha incorreto.', Response::HTTP_UNAUTHORIZED);
            }
            return response()->json([
                'access_token' => $usuario->createToken('apiToken')->plainTextToken
            ], Response::HTTP_CREATED);
        } catch (\Throwable $th) {
            return response()->json([
                'erro' => true,
                'message' => $th->getMessage()
            ], $th->getCode() != 1 ? $th->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR);
        }
            
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response([], Response::HTTP_NO_CONTENT);
    }

    public function perfil()
    {
        $usuario = Usuario::with([
            'departamento' => fn($query) => $query->select(
                'id',
                'departamento_id',
                'tipos_departamento_id',
                'departamento_externo',
                'nome'
            ),
            'departamento.departamento' => fn($query) => $query->select(
                'id',
                'nome'
            ),
            'departamento.tiposDepartamento' => fn($query) => $query->select(
                'id',
                'descricao'
            )
        ])->select('id', 'departamento_id', 'permissao_id', 'usuario', 'nome', 'cpf')
        ->findOrFail(auth()->user()->id);
        return response()->json(['usuario' => $usuario]);
    }
}
