<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditProtocoloRequest;
use App\Http\Requests\ProtocoloRequest;
use App\Http\Resources\ProtocoloResource;
use App\Models\Movimentacao;
use App\Models\Protocolo;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProtocoloController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->perPage ?? 10;
        try {
            return new ProtocoloResource(
                Protocolo::with([
                    'statusProtocolo' => fn($query) => $query->select(
                        'id',
                        'descricao'
                    ),
                    'origem' => fn($query) => $query->select(
                        'id',
                        'origem'
                    ),
                    'primeiraMovimentacao' => fn($query) => $query->select(
                        'id',
                        'protocolo_id',
                        'departamento_origem_id',
                        'usuario_id'
                    ),
                    'primeiraMovimentacao.departamentoOrigem' => fn($query) => $query->select(
                        'id',
                        'nome'
                    ),
                    'primeiraMovimentacao.usuario' => fn($query) => $query->select(
                        'id',
                        'nome'
                    )
                ])
                ->where('status_protocolo_id', 2)
                ->orderBy('id', 'DESC')
                ->paginate($page)
            );
        } catch (\Throwable $th) {
            return response()->json(['erro' => true, 'message' => $th->getMessage()]);
        }
    }

    public function movimentacaoInsert(Protocolo $protocolo, ProtocoloRequest $request)
    {
        $protocolo->movimentacao()->create([
            'usuario_id' => auth()->user()->id,
            'departamento_origem_id' => auth()->user()->departamento_id,
            'departamento_destino_id' => $request->departamento_destino_id
        ]);
    }

    public function geraCodigo()
    {
        $data = \Carbon\Carbon::now(new DateTimeZone('America/Fortaleza'))->format('Ym');
        $id = !empty(Protocolo::orderBy('id', 'desc')->first()) && Protocolo::latest()->first()->count() > 0 
            ? Protocolo::latest()->first()->id + 1 
            : 1;
        return $data . "-" . $id;
    }

    public function store(ProtocoloRequest $request)
    {
        try {
            $request->merge([
                'status_protocolo_id' => 2,
                'codigo' => $this->geraCodigo()
            ]);
            // //inseri os dados na tabela protocolos
            if($request->origem == 1) $request->origem_outro;
            $protocolo = Protocolo::create($request->all());
            //inseri os dados na tabela movimentacoes
            $this->movimentacaoInsert($protocolo, $request);
            // return new ProtocoloResource(Protocolo::with(['movimentacao' => function($query) use ($protocolo){
            //     $query->where('protocolo_id', $protocolo->id);
            // }])->find($protocolo->id));
            return $this->show($protocolo->id);
        } catch (\Throwable $th) {
            return response()->json(['erro' => $th->getMessage()], 422);
        }
    }

    public function show($id)
    {
        try {
            return new ProtocoloResource(
                Protocolo::with(['movimentacao' => fn($query) => $query->select(
                    'id',
                    'usuario_id',
                    'protocolo_id',
                    'departamento_origem_id',
                    'departamento_destino_id',
                    'descricao',
                ),
                'movimentacao.movimentacaoDepartamento',
                'movimentacao.movimentacaoDepartamento.tiposDepartamento' => fn($query) => $query->select(
                    'id',
                    'descricao'
                ),
                'movimentacao.movimentacaoDepartamento.departamento' => fn($query) => $query->select(
                    'id',
                    'nome'
                ),
                'movimentacao.usuario' => fn($query) => $query->select(
                    'id',
                    'nome'
                ),
                'primeiraMovimentacao' => fn($query) => $query->select(
                    'id',
                    'protocolo_id',
                    'departamento_origem_id',
                    'usuario_id'
                ),
                'primeiraMovimentacao.departamentoOrigem' => fn($query) => $query->select(
                    'id',
                    'nome'
                ),
                'primeiraMovimentacao.usuario' => fn($query) => $query->select(
                    'id',
                    'nome'
                ),
                'ultimaMovimentacao',
                'ultimaMovimentacao.departamentoDestino' => fn($query) => $query->select(
                    'id',
                    'departamento_id',
                    'tipos_departamento_id',
                    'nome',
                ),
                'ultimaMovimentacao.departamentoDestino.departamento' => fn($query) => $query->select(
                    'id',
                    'nome',
                ),
                'ultimaMovimentacao.departamentoDestino.tiposDepartamento' => fn($query) => $query->select(
                    'id',
                    'descricao',
                ),
                'statusProtocolo' => fn($query) => $query->select(
                    'id',
                    'descricao',
                )])
                ->select([
                    'id',
                    'status_protocolo_id',
                    'codigo',
                    'descricao',
                    'resolucao',
                    'origem_outro',
                ])
                ->findOrFail($id)
            );
        } catch (\Throwable $th) {
            return response()->json(['error' => true, 'message' => $th->getMessage()]);
        }
    }

    public function finalizaProtocolo(Request $request, $id)
    {
        try {
            $request->validate([
                'resolucao' => 'required'
            ], [
                'resolucao.required' => 'Resolução obrigatório'
            ]);
            $protocolo = Protocolo::find($id);
            if (!$protocolo) return throw new Exception("Protocolo não encontrado", 404);
            if ($protocolo->status_protocolo_id != 2) return throw new Exception("Protocolo encerrado /ou arquivado.", 403);
            $protocolo->update([
                'status_protocolo_id' => 3,
                'resolucao' => $request->resolucao
            ]);
            return $this->show($id);
        } catch (\Throwable $th) {
            return response()->json([
                'erro' => true,
                'message' => $th->getMessage()
            ], $th->getCode() == 0 ? 500 : $th->getCode());
        }
    }

    public function encaminhaProtocolo(Request $request, $id)
    {
        try {
            $request->validate([
                'departamento_destino_id' => 'required',
                'descricao' => 'required'
            ], [
                'departamento_destino_id.required' => 'Departamento destino obrigatório',
                'descricao.required' => 'Descrição obrigatório'
            ]);
            //Buca o protocolo no banco
            $protocolo = Protocolo::with('ultimaMovimentacao')->find($id);
            //Caso não encontre o protocolo retorna a mensagem com o status
            if (!$protocolo) return throw new Exception("Protocolo não encontrado", 404);
            //Verifica se o status do protocolo é diferente de aguardando análise
            if ($protocolo->status_protocolo_id != 2) return throw new Exception("Protocolo encerrado /ou arquivado.", 403);
            //Verifica se o departamento destino é externo, caso sim atualiza o protocolo com status finalizado
            if ($request->departamento_destino_id == 8) $protocolo->update(['status_protocolo_id' => 3]);
            //Inseri um novo registro na tabela movimentações
            $protocolo->movimentacao()->create([
                'usuario_id' => auth()->user()->id,
                'protocolo_id' => $id,
                'departamento_origem_id' => $protocolo->ultimaMovimentacao->departamento_destino_id,
                'departamento_destino_id' => $request->departamento_destino_id,
                'descricao' => $request->descricao
            ]);
            //Retorna o protocolo
            return $this->show($id);
        } catch (\Throwable $th) {
            return response()->json([
                'erro' => true,
                'message' => $th->getMessage()
            ], $th->getCode() == 0 ? 500 : $th->getCode());
        }
    }

    public function protocolosPorSetor()
    {
        try {
            $departamentoUsuario = auth()->user()->departamento_id;
            return new ProtocoloResource(
                Protocolo::with([
                    'ultimaMovimentacao',
                    'statusProtocolo' => fn($query) => $query->select(
                        'id',
                        'descricao'
                    ),
                    'origem' => fn($query) => $query->select(
                        'id',
                        'origem'
                    ),
                    'primeiraMovimentacao' => fn($query) => $query->select(
                        'id',
                        'protocolo_id',
                        'departamento_origem_id',
                        'usuario_id'
                    ),
                    'primeiraMovimentacao.departamentoOrigem' => fn($query) => $query->select(
                        'id',
                        'nome'
                    ),
                    'primeiraMovimentacao.usuario' => fn($query) => $query->select(
                        'id',
                        'nome'
                    )
                ])->whereHas(
                    'ultimaMovimentacao', fn($query) => $query->where('departamento_destino_id', '=', $departamentoUsuario)
                )->where('status_protocolo_id', 2)
                ->get()
            );
        } catch (\Throwable $th) {
            return response()->json(['erro' => true, 'message' => $th->getMessage()]);
        }
    }

    public function updateProtocolo(EditProtocoloRequest $request, $id)
    {
        try {
            $protocolo = Protocolo::findOrFail($id);
            if ($protocolo->status_protocolo_id != 2) {
                return throw new Exception("Protocolo encerrado /ou arquivado.", Response::HTTP_FORBIDDEN);
            }
            $protocolo->update($request->all());
            return $this->show($protocolo->id);
        } catch (\Throwable $th) {
            return response()->json([
                'erro' => true,
                'message' => $th->getMessage()
            ], $th->getCode() == 0 ? 500 : $th->getCode());
        }
    }

    public function pesquisaProtocolo(Request $request)
    {
        $filtro = Protocolo::filtro($request)->with('origem')->get();
        // $teste = Protocolo::with(['origem' => fn($query) => $query->where(
        //     'origem', 'LIKE', '%'.$request->origem .'%'
        // )])->whereHas('origem', fn($query) => $query->where('origem', 'LIKE', '%'.$request->origem .'%'))
        // ->get();
        // dd($filtro);
        return response()->json(['data' => $filtro]);
    }

    public function protocolosFinalizadoPorSetor()
    {
        try {
            $departamentoUsuario = auth()->user()->departamento_id;
            return new ProtocoloResource(
                Protocolo::with([
                    'ultimaMovimentacao',
                    'statusProtocolo' => fn($query) => $query->select(
                        'id',
                        'descricao'
                    ),
                    'origem' => fn($query) => $query->select(
                        'id',
                        'origem'
                    ),
                    'primeiraMovimentacao' => fn($query) => $query->select(
                        'id',
                        'protocolo_id',
                        'departamento_origem_id',
                        'usuario_id'
                    ),
                    'primeiraMovimentacao.departamentoOrigem' => fn($query) => $query->select(
                        'id',
                        'nome'
                    ),
                    'primeiraMovimentacao.usuario' => fn($query) => $query->select(
                        'id',
                        'nome'
                    )
                ])->whereHas(
                    'ultimaMovimentacao', fn($query) => $query->where('departamento_destino_id', '=', $departamentoUsuario)
                )->where('status_protocolo_id', 3)
                ->get()
            );
        } catch (\Throwable $th) {
            return response()->json(['erro' => true, 'message' => $th->getMessage()]);
        }
    }
}
