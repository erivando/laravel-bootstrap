<?php

namespace App\Http\Controllers;

use App\Http\Resources\DepartamentoResource;
use App\Models\Departamentos;
use Illuminate\Http\Request;

class DepartamentosController extends Controller
{
    public function index()
    {
       $departamentoId = [];
       
       if (auth()->user()->permissao_id == 2) {
           array_push($departamentoId, auth()->user()->departamento_id);
        } else {
           array_push($departamentoId, auth()->user()->departamento_id, 14);
       }
    
        return new DepartamentoResource(
            Departamentos::with(
            [
                'departamento' => function($query) {
                    $query->select('id', 'tipos_departamento_id', 'nome');
                },
                'tiposDepartamento' => function($query) {
                    $query->select('id', 'descricao');
                },
                'departamento.tiposDepartamento' => function($query) {
                    $query->select('id', 'descricao');
                }
            ])
            ->whereNotIn('id', $departamentoId)
            ->select('id', 'departamento_id', 'tipos_departamento_id', 'departamento_externo', 'nome')
            ->get()
        );
    }
}
