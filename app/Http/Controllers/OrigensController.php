<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrigemResource;
use App\Models\Origens;
use Illuminate\Http\Request;

class OrigensController extends Controller
{
    public function index()
    {
        return new OrigemResource(Origens::select('id', 'origem')->get());
    }
}
