<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class RelatorioController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Relatorio pdf',
            'date' => date('m/d/Y')
        ];
        $pdf = App::make('dompdf.wrapper')->loadView('relatorio.relatorio', $data);
        return $pdf->stream('teste.pdf');
    }
}
