<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProtocoloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $origem_id = $this->request->get('origem_id');

        $rules = [
            'origem_id' => 'required',
            'departamento_destino_id' => 'required',
            'descricao' => 'required|string'
        ];

        if ($origem_id == 1) {
            $rules['origem_outro'] = 'required';
        }

        return $rules;
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validator errors',
            'data' => $validator->errors()
        ]));
    }

    public function messages()
    {
        return [
            'origem_id.required' => 'Origem do protocolo obrigatório',
            'departamento_destino_id.required' => 'Departamento destino obrigatório',
            'descricao.required' => 'Descrição obrigatório',
            'descricao.string' => 'Descrição deve ser string',
            'origem_outro.required' => 'Origem outro obrigatório'
        ];
    }
}
