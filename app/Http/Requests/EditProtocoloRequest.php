<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

class EditProtocoloRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $rules = [
            'origem_id' => 'required',
            'descricao' => 'required|string'
        ];

        if ($this->request->get('origem_id') == 1) {
            $rules['origem_outro'] = 'required';
        }

        return $rules;
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validator erros',
            'data' => $validator->errors()
        ], Response::HTTP_BAD_REQUEST));
    }

    public function messages()
    {
        return [
            'origem_id.required' => 'Origem do protocolo obrigatória',
            'descricao.required' => 'Descrição obrigatório',
            'descricao.string' => 'Descrição deve ser uma string',
            'origem_outro' => 'Outro obrigatório'
        ];
    }
}
