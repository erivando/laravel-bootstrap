<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="container">
            <h1>teste</h1>
            <button id="teste" class="btn btn-primary" type="button" data-toggle="modal" data-target="#myModal">
                <i class="bi bi-plus"></i>
                Adicionar
            </button>
            <div class="modal" tabindex="-1" id="myModal" data-bs-keyboard="false">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Novo Processo</h5>
                      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="" id="formProtocolo">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Origem</label>
                                        <select class="form-select mb-3" id="origens" name="origens" aria-label="Default select example">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="mb-3">
                                        <label for="exampleFormControlInput1" class="form-label">Data de Recebimento</label>
                                        <input type="text" readonly class="form-control" value="08/12/2022" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="exampleFormControlTextarea1" class="form-label">Outro</label>
                                    <div class="mb-3">
                                        <label for="exampleFormControlInput1" class="form-label visually-hidden"></label>
                                        <input type="text" class="form-control" value="Outro" id="exampleFormControlInput1" placeholder="Digite aqui de onde é a solicitação">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label for="exampleFormControlInput1" class="form-label">De</label>
                                        <input type="text" readonly class="form-control" value="Protocolo" id="exampleFormControlInput1" placeholder="name@example.com">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="exampleFormControlTextarea1" class="form-label">Para</label>
                                    <select id="departamentos" class="form-select mb-3" aria-label="Default select example">
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Tipo de Documento / Assunto</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#teste").click(function() {
                    $('#myModal').modal('show')
                    listaOrigem()
                    listaDepartamentos()
                })
                function listaOrigem() {
                    $.ajax({
                        url: '/origens',
                        dataType: 'json',
                        cache: false,
                        beforeSend: function() {
                            $('#origens').attr('disabled', true)
                        },
                        success: function(data) {
                            let dados = data.data
                            $.each(dados, function(res){
                                $('#origens').append($('<option></option>').val(dados[res].id).text(dados[res].origem))
                            })
                        }
                    }).done(function() {
                        $('#origens').attr('disabled', false)
                    })
                }
                function listaDepartamentos() {
                    $.ajax({
                        url: '/departamentos',
                        dataType: 'json',
                        cache: false,
                        beforeSend: function() {
                            $('#departamentos').attr('disabled', true)
                        },
                        success: function(data) {
                            let dados = data.data
                            $.each(dados, function(res){
                                $('#departamentos').append($('<option></option>').val(dados[res].id).text(dados[res].nome))
                            })
                        }
                    }).done(function() {
                        $('#departamentos').attr('disabled', false)
                    })
                }

                $('#formProtocolo').on('submit', function(ev) {
                    $('#myModal').modal({
                        show: 'false'
                    }); 


                    var data = $(this).serializeObject();
                    json_data = JSON.stringify(data);
                    // $("#results").text(json_data); 
                    // $(".modal-body").text(json_data); 
                    console.log(json_data)
                    // $("#results").text(data);

                    // ev.preventDefault();
                });
            });
        </script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    </body>
</html>
