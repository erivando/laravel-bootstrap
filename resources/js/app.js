import { createApp } from "vue"
import App from "./src/App.vue"

import '@coreui/coreui/dist/css/coreui.min.css'

createApp(App).mount('#app')
